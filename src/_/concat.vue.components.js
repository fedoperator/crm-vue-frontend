
Vue.component('common-app', {
	
	template : '#common-app',
	
	props : [
		'appstate',
	],
	
	data : function () {
		return {
			intervals : {
				session_update : null,
			}
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});
		
		//vueData._.loadSession();
		var _t = this;
		
		this.intervals.session_update = setInterval(function(){
			
			_t.$root.callApi('session/get', null, function(resp){});
			
		}, 3000);
		
	},
	
	mounted: function () {
		
		this.$nextTick(function () {
			//после монтирования всех потомков в дерево DOM
		});
		
	},
	
	methods : {
		
		/*
		loginSet : function(id) {
			
			id = id || 0;
			
			if(id) {
				vueData._.loadSession();
			} else {
				vueData._.logout();
			}
			
			//this.appdata.session.profile.id = id;
			
		},
		*/
		
	},
	
});

/*
    
//    components : {
//       'default-component' : 'default-component',
//    },


 
computed : {
	full_name: function(){
		return this.first_name + this.last_name; //Вася Пупкин   
	},
},

methods : {
	
	// методы жизненного цикла
	beforeCreate : function() {

	},
	created : function() {

	},
	beforeMount : function() {

	},
	mounted : function() {

	},
	beforeUpdate : function() {

	},
	updated : function() {

	},
	beforeDestroy : function() {

	},
	destroyed : function() {

	},

},
*/

Vue.component('common-footer', {

	template : '#common-footer',

	props : [
		'appstate',
	],

	data : function () {
		return {
			
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});

	},

});

Vue.component('common-header', {
	
	template : '#common-header',

	props : [
		'appstate',
	],

	data : function () {
		return {
			
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});
		
	},
	
	methods : {
		
		login_unset : function() {
			
			var _t = this;
			
			this.$root.callApi('session/logout', {}, function(resp){
				
				if(resp && resp.session && resp.session.profile == null) {
					
					_t.$store.commit('pageSessionLoad', null);
					
				} else {
					
					
					_t.$store.commit('log', {
						wrong_logout : resp,
					});
					
				}
				
			});
			this.$store.state.session.profile = null;
			
		},
		
	}
	
});

Vue.component('common-loading', {
	
	template : '#common-loading',
	
	props : [
		'appstate',
	],
	
	data : function () {
		return {
			
		}
	},
	
	created : function() {
		
		var _s = this.$store;
		
		_s.commit('log', {
			created : this.$options.template,
		});
		
		this.$root.callApi('session/get', null, function(resp){
			
			_s.commit('pageLoadingStop', resp.session.profile);
			
		});
		
	},
	
});

Vue.component('page-login', {
	
	template : '#page-login',

	props : [
		'appstate',
	],
	
	data : function () {
		return {
			form : {
				login : '',
				pass : '',
			},
			errors : {
				empty_login : false,
				empty_pass : false,
				wrong_access : false,
			},
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});
		
	},
	
	watch : {
		
		
		
	},
	
	methods : {
		
		sendForm : function(event) {
			
			//this.$els.custom.submit();
			this.errors.empty_login = false;
			this.errors.empty_pass = false;
			this.errors.wrong_access = false;
			
			if(!this.form.login || this.form.login == '') {
				
				this.errors.empty_login = true;
				
			}
			if(!this.form.pass || this.form.pass == '') {
					
				this.errors.empty_pass = true;
				
			}
			
			if(!this.errors.empty_login && !this.errors.empty_pass) {
				
				var _t = this;
				
				var p = {
					login : this.form.login,
					pass : this.form.pass,
				};
				
				this.$root.callApi('session/login', p, function(resp){
					
					if(resp && resp.session && resp.session.profile && resp.session.profile.id) {
						
						_t.$store.commit('pageSessionLoad', resp.session.profile);
						
					} else {
						
						_t.errors.wrong_access = true;
						
						_t.$store.commit('log', {
							wrong_access : p,
						});
						
					}
					
				});
				
			}
			
		},
		
	}
	
});