
Vue.component('common-app', {
	
	template : '#common-app',
	
	props : [
		'appstate',
	],
	
	data : function () {
		return {
			intervals : {
				session_update : null,
			}
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});
		
		//vueData._.loadSession();
		var _t = this;
		
		this.intervals.session_update = setInterval(function(){
			
			_t.$root.callApi('session/get', null, function(resp){});
			
		}, 3000);
		
	},
	
	mounted: function () {
		
		this.$nextTick(function () {
			//после монтирования всех потомков в дерево DOM
		});
		
	},
	
	methods : {
		
		/*
		loginSet : function(id) {
			
			id = id || 0;
			
			if(id) {
				vueData._.loadSession();
			} else {
				vueData._.logout();
			}
			
			//this.appdata.session.profile.id = id;
			
		},
		*/
		
	},
	
});

/*
    
//    components : {
//       'default-component' : 'default-component',
//    },


 
computed : {
	full_name: function(){
		return this.first_name + this.last_name; //Вася Пупкин   
	},
},

methods : {
	
	// методы жизненного цикла
	beforeCreate : function() {

	},
	created : function() {

	},
	beforeMount : function() {

	},
	mounted : function() {

	},
	beforeUpdate : function() {

	},
	updated : function() {

	},
	beforeDestroy : function() {

	},
	destroyed : function() {

	},

},
*/