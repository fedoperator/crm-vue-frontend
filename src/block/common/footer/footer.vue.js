
Vue.component('common-footer', {

	template : '#common-footer',

	props : [
		'appstate',
	],

	data : function () {
		return {
			
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});

	},

});