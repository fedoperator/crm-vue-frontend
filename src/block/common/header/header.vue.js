
Vue.component('common-header', {
	
	template : '#common-header',

	props : [
		'appstate',
	],

	data : function () {
		return {
			
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});
		
	},
	
	methods : {
		
		login_unset : function() {
			
			var _t = this;
			
			this.$root.callApi('session/logout', {}, function(resp){
				
				if(resp && resp.session && resp.session.profile == null) {
					
					_t.$store.commit('pageSessionLoad', null);
					
				} else {
					
					
					_t.$store.commit('log', {
						wrong_logout : resp,
					});
					
				}
				
			});
			this.$store.state.session.profile = null;
			
		},
		
	}
	
});