
Vue.component('common-loading', {
	
	template : '#common-loading',
	
	props : [
		'appstate',
	],
	
	data : function () {
		return {
			
		}
	},
	
	created : function() {
		
		var _s = this.$store;
		
		_s.commit('log', {
			created : this.$options.template,
		});
		
		this.$root.callApi('session/get', null, function(resp){
			
			_s.commit('pageLoadingStop', resp.session.profile);
			
		});
		
	},
	
});