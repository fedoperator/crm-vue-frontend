
Vue.component('page-login', {
	
	template : '#page-login',

	props : [
		'appstate',
	],
	
	data : function () {
		return {
			form : {
				login : '',
				pass : '',
			},
			errors : {
				empty_login : false,
				empty_pass : false,
				wrong_access : false,
			},
		}
	},
	
	created : function() {
		
		this.$store.commit('log', {
			created : this.$options.template,
		});
		
	},
	
	watch : {
		
		
		
	},
	
	methods : {
		
		sendForm : function(event) {
			
			//this.$els.custom.submit();
			this.errors.empty_login = false;
			this.errors.empty_pass = false;
			this.errors.wrong_access = false;
			
			if(!this.form.login || this.form.login == '') {
				
				this.errors.empty_login = true;
				
			}
			if(!this.form.pass || this.form.pass == '') {
					
				this.errors.empty_pass = true;
				
			}
			
			if(!this.errors.empty_login && !this.errors.empty_pass) {
				
				var _t = this;
				
				var p = {
					login : this.form.login,
					pass : this.form.pass,
				};
				
				this.$root.callApi('session/login', p, function(resp){
					
					if(resp && resp.session && resp.session.profile && resp.session.profile.id) {
						
						_t.$store.commit('pageSessionLoad', resp.session.profile);
						
					} else {
						
						_t.errors.wrong_access = true;
						
						_t.$store.commit('log', {
							wrong_access : p,
						});
						
					}
					
				});
				
			}
			
		},
		
	}
	
});