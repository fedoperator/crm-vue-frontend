'use strict';

Vue.config.silent = false;
Vue.config.productionTip = false;
Vue.config.devtools = true;

var vueApp = new Vue({
	el : '#vueApp',
	template : `
		<div>
			<common-loading v-bind:appstate="appstate" v-if="isloading" ></common-loading>
			<common-app v-bind:appstate="appstate" v-else-if="isauth" ></common-app>
			<page-login v-bind:appstate="appstate" v-else ></page-login>
		</div>
	`,
	replace : true,
	data : {},//vueData,
	computed : {
		appstate : function() {
			return this.$store.state;
		},
		isloading : function() {
			return this.$store.state && this.$store.state.flags && this.$store.state.flags.pageLoading;
		},
		isauth : function() {
			return this.$store.state && this.$store.state.session && this.$store.state.session.profile && this.$store.state.session.profile.id;
		},
	},
	router : new VueRouter({
		routes : vueData.state.routes,
	}),
	store : vueData,
	methods : {
		
		callApi : function(path, params, cb) {
			
			var _t = this;
			
			path = path || 'default';
			
			path = path + '.json';
			
			axios
				.get('/api/v1/' + path)
				.then(function(response) {
					
					//vueData.appdata = response.data;
					//vueData.appdata = Object.assign({}, vueData.appdata, response.data);
					//vueData.replaceState(response.data);
					
					/*
					for(var k in response.data) {
						vueData.state[k] = _t.$root.mergeObjects(vueData.state[k], response.data[k]);
					}
					*/
					
					cb && cb(response.data);
					
				})
				.catch(function(error) {
					
					console.error(error);
					
				})
				.finally(function() {
					
				})
			;
			
		},
		
		mergeObjects : function() {
			
			var res = {};
			
			for(var i = 0; i < arguments.length; i++) {
				
				var o = arguments[i];
				
				for(var k in o) {
					try {
						if(o.constructor == Object) {
							res[k] = this.mergeObjects(res[k], o[k]);
						} else {
							res[k] = o[k];
						}
					} catch(e) {
						res[k] = o[k];
					}
				}
				
			}
			
			return res;
			
		},
		
	},
	
});
