'use strict';

var vueData = new Vuex.Store({
	//https://vuex.vuejs.org/ru/guide/state.html
	//https://habr.com/post/322354/
	state: {
		errors : {
			
		},
		flags : {
			pageLoading : true,
		},
		session : {
			profile : null,
		},
		routes : [
			{
			"title" : "Главная",
			"path" : "/mainpage/",
			"component" : {
				"template" : "<div>mainpage</div>"
			}
		},
		{
			"title" : "Входящие",
			"path" : "/incoming/items/",
			"component" : {
				"template" : "<div>incoming/items</div>"
			}
		}
		],
	},
	actions : {
		loginStart : function(context) {
			//commit('api__request', note);
		},
		session_get : function(context) {
			
		},
	},
	mutations : {
		log : function(state, data) {
			console.log(new Date().getTime(), data);
		},
		pageLoadingStop : function(state, data) {
			state.flags.pageLoading = false;
			if(data && data.id) {
				state.session.profile = data;
			}
		},
		pageSessionLoad : function(state, data) {
			state.session.profile = data
		},
	},
	getters : {
		
	},  
	modules : {

	},
});

/*
var vueData = {
	appdata : {
		loading : true,
		routes : [
			{
				title : 'Главная',
				path : '/mainpage/',
				component : {
					template : '<div>mainpage</div>',
				}
			},
			{
				title : 'Входящие',
				path : '/incoming/items/',
				component : {
					template : '<div>incoming/items</div>',
				}
			},
		],
		errors : {
			session : {

			},
		},
	},
	_ : {
		intervals : {
			getSession : null,
		},
		log : function(data) {
			
			console.log(new Date().getTime(), data);

		},
		api : function(path, params, cb) {

			path = path || 'default';

			path = path + '.json';

			axios
				.get('/api/v1/' + path)
				.then(function(response) {
					
					vueData.appdata = response.data;
					//vueData.appdata = Object.assign({}, vueData.appdata, response.data);

					cb && cb(response);

				})
				.catch(function(error) {
					console.debug(error);
				})
				.finally(function() {

				});

		},
		loadSession : function() {

			vueData._.api('session/get', null, function(data){
				if(!vueData._.intervals.getSession) {
					vueData._.intervals.getSession = setInterval(vueData._.loadSession, 10000);
				}
			});

		},
		logout : function() {

			vueData._.api('session/logout', null, function(data){
				clearInterval(vueData._.intervals.getSession);
			});

		},
		loginStart : function(login, pass) {

			vueData._.api('session/login', null, function(data){
				vueData._.loadSession();
			});

		},
	},
};
*/